import unittest
#
from calculator import add

class TestAddition(unittest.TestCase):

    def test_positive_numbers(self):
        numA, numB = 1, 2
        result = 3
        self.assertEqual(add(numA, numB), result)

    def test_negative_number(self):
        numA, numB = 1, -2
        result = -1
        self.assertEqual(add(numA, numB), result)

    def test_invalid_input_string(self):
        numA, numB = 1, "hello"
        with self.assertRaises(ValueError):
            add(numA, numB)

if __name__ == '__main__':
    unittest.main()
